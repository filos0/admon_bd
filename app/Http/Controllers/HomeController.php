<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class HomeController extends Controller
{
    public function home(Request $Request){
     	
		if (!Session::has('usuario')) {
		 return view('login');
		}

		return view('welcome');
     
    }
}
