<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class LoginController extends Controller
{
    public function login(Request $request){
     
     	/*$valido = $this->limpiar($request->toArray());
     	if (!$valido) 
     		return view('login')->withErrors("Tecleaste cosas raras ...");
     	*/
        $bd = $this->checkdatabase($request->toArray());

        if (!$bd) {
           return view('login')->withErrors("Usuario/Contraseña Incorrectos");
        }
        
		else{
			$this->crear_sesion($request->toArray());
			return redirect()->route('home');
		}
    }

    private function limpiar($request){
   
    	$valido = true;
    	$strings = array($request["contraseña"], $request["usuario"]);
		foreach ($strings as $testcase) {
		    if (!ctype_alnum($testcase)) {       
		        $valido = false;
		    }
		}
		return $valido;
		
    }
    private function checkdatabase($request){
        
        try {
        config(['database.connections.sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => '127.0.0.1',
            'port' => '1433',
            'database' => 'Casa',
            'username' => $request["usuario"],
            'password' => $request["contraseña"],
        ]]);
            
        //DB::connection()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        DB::connection()->getPdo();
        return true;
            
            
        } catch (\PDOException $e) {
            
            return false;
        }
    }

    private function crear_sesion($datos){
    	
 
    	Session::put('nombre', "Nombre_Prueba"); 
        Session::put('apellido', ""); 

        Session::put('usuario', $datos["usuario"]);
        Session::put('contraseña', $datos["contraseña"]);
    }
    public function logout(){
    	Session::flush();
    	return redirect()->route('home');

    }
}
