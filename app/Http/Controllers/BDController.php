<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class BDController extends Controller
{
      public function ver_reservaciones(Request $Request){
     	
		if (!Session::has('usuario')) {
		 return view('login');
		}

		config(['database.connections.sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => '127.0.0.1',
            'port' => '1433',
            'database' => 'Casa',
            'username' => Session::get('usuario'),
            'password' => Session::get('contraseña'),
        ]]);
		$Reservaciones = DB::select("Select nombrecliente, apellidocliente, fechareservacion,telefonocliente, nivel,horainicio
			FROM cliente 
			inner join reservacion on cliente.idcliente=reservacion.IdCliente
			inner join servicio on reservacion.idservicio= servicio.idservicio
			group by cliente.idcliente,nombrecliente, apellidocliente, fechareservacion,telefonocliente, nivel,horainicio");

		return view("reservacion")->with('Reservaciones',$Reservaciones);
     
    }

	 public function reservar(Request $Request){
	     	
			if (!Session::has('usuario')) {
			 return view('login');
			}
			config(['database.connections.sqlsrv' => [
    		'driver' => 'sqlsrv',
            'host' => '127.0.0.1',
            'port' => '1433',
            'database' => 'Casa',
            'username' => Session::get('usuario'),
            'password' => Session::get('contraseña'),
        	]]);
			
			$tipo_pago = DB::select("Select * from TipoPago");
			$estado_reservacion = DB::select("Select * from EstadoReservacion");
			$servicios = DB::select("Select * from Servicio");
			$tarifa = DB::select("Select * from Tarifa");
		
			return view("reservar")
			->with('tipos_pago',$tipo_pago)
			->with('estado_reservacion',$estado_reservacion)
			->with('servicios',$servicios)
			->with('tarifas',$tarifa);
	     
	    }

}
