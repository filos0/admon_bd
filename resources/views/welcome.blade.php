@extends('master')
@section('content')

<div class="panel">
    <div class="panel-header">
        <div class="panel-heading text-center">
            <label style="font-size: 26px" class="text-bold text-teal-800">Bienvenidos al Sistema</label>
        </div>
    </div>
    <div class="panel-body">
        <div class="row" style="margin:30px">
             <label style="font-size: 18px" class="text-bold">HOLA {{Session::get('nombre')}} {{Session::get('apellido')}}</label>
        </div>
    </div>
</div>

@endsection