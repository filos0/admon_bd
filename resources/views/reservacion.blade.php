@extends('master')
@section('content')

<div class="panel">
    <div class="panel-header">
        <div class="panel-heading text-center">
            <label style="font-size: 26px" class="text-bold text-teal-800">Reservaciones</label>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <table class="table table-bordered">
                 <thead class="bg-teal-800">
                     <tr>
                         <th style="font-size: 18px" class="text-bold text-center">Nombre Completo</th>
                         <th style="font-size: 18px" class="text-bold text-center">Nivel</th>
                         <th style="font-size: 18px" class="text-bold text-center">Fecha</th>
                         <th style="font-size: 18px" class="text-bold text-center">Hora</th>
                     </tr>
                 </thead>
                 <tbody>
                     @foreach ($Reservaciones as $Reservacion)
                         <tr style="font-size: 16px">
                             <td class="text-center">{{$Reservacion->nombrecliente}} {{$Reservacion->apellidocliente}}</td>
                             <td class="text-center">{{$Reservacion->nivel}}</td>
                             <td class="text-center">{{date('d-m-Y',strtotime($Reservacion->fechareservacion))}}</td>
                             <td class="text-center">{{date('h:i',strtotime($Reservacion->horainicio))}}</td>
                            
                         </tr>
                     @endforeach
                 </tbody>
             </table>
            </div>
        </div>
    </div>
</div>

@endsection