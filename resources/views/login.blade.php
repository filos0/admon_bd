@extends('master')
@section('content')

<div class="col-md-12">
    <div class="col-md-6 col-md-offset-3">
         @if(!empty($errors->first()))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <span>{{ $errors->first() }}</span>
                    </div>
                </div>
            </div>
        @endif
        <form method="post" action="{{url('/login')}}">
            {{csrf_field()}}
            <div class="panel">
                <div class="panel-header">
                    <div class="panel-heading">
                        <div class="text-center">
                            <label style="font-size: 22px;" class="text-teal-800 text-bold">
                            Inicio de Sesión
                            </label>
                        </div>
                    </div>
                </div>  

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-10 col-md-offset-1 ">    
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="text" name="usuario" class="form-control input-lg"
                                    placeholder="Usuario" required autofocus>
                                    <div class="form-control-feedback">
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-10 col-md-offset-1 ">    
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" name="contraseña" class="form-control input-lg"
                                    placeholder="Contraseña" required>
                                    <div class="form-control-feedback">
                                        <i class="icon-lock"></i>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="text-center">
                            <button type="submit" class="btn btn-xlg bg-teal-800">
                                Entrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>

@endsection