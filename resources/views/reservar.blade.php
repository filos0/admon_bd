@extends('master')
@section('content')

<div class="panel">
    <div class="panel-header">
        <div class="panel-heading text-center">
            <label style="font-size: 26px" class="text-bold text-teal-800">Reservar</label>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            
                <div class="col-md-4">
                    <div class="form-group">
                        <label style="font-size: 16px" class="label-control col-md-4 text-bold">Fecha de Reservación</label>
                        <div class="col-md-8">
                            <input class="form-control" type="date" name="fecha_reservacion" min="{{date('Y-m-d')}}" autofocus>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label style="font-size: 16px" class="label-control col-md-4 text-bold">Núm personas</label>
                        <div class="col-md-8">
                            <select class="form-control" name="tarifa">
                                <option class="form-control"></option>
                                @foreach($tarifas as $tarifa)
                                    <option class="form-control" value="{{$tarifa->IdTarifa}}">{{$tarifa->NoPersonas}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                  <div class="col-md-4">
                    <div class="form-group">
                        <label style="font-size: 16px" class="label-control col-md-4 text-bold">Precio</label>
                        <div class="col-md-8">
                           <input class="form-control text-bold" type="text" id="precio" readonly>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('[name = tarifa]').on('change',function(){

        if ($(this).val() === "1" ) 
            $('#precio').val("$80.00")
        
        else if ($(this).val() === "2" ) 
            $('#precio').val("$120.00")
        
        else if ($(this).val() === "3" ) 
            $('#precio').val("$150.00")
        
        else 
            $('#precio').val("")
    })
</script>
@endsection